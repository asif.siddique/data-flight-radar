# Databricks notebook source
from FlightRadar24 import FlightRadar24API
from FlightRadar24.errors import CloudflareError
import pyspark.sql.functions as F
import time
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DoubleType, ShortType, LongType

# COMMAND ----------

class FlightRadarFunction:
    fr_api = FlightRadar24API()

    def split_coordinates2(self, y1, y2, x1, x2, rows, cols):
        lat_range = (y2 - y1) / rows
        lon_range = (x2 - x1) / cols
    
        rectangles = []
        for i in range(rows):
            for j in range(cols):

                rect_y1 = round(y1 + i * lat_range, 6)
                rect_y2 = round(y1 + (i + 1) * lat_range, 6)
                rect_x1 = round(x1 + j * lon_range, 6)
                rect_x2 = round(x1 + (j + 1) * lon_range, 6)
    
                rectangle_str = f"{rect_y1},{rect_y2},{rect_x1},{rect_x2}"
                rectangles.append(rectangle_str)
        return rectangles
    
    def get_flights_list(self, boundlist):
        data_schema = StructType(
            [
                StructField("aircraft_code", StringType(), True),
                StructField("airline_iata", StringType(), True),
                StructField("airline_icao", StringType(), True),
                StructField("altitude", LongType(), True),
                StructField("callsign", StringType(), True),
                StructField("destination_airport_iata", StringType(), True),
                StructField("ground_speed", LongType(), True),
                StructField("heading", LongType(), True),
                StructField("icao_24bit", StringType(), True),
                StructField("id", StringType(), True),
                StructField("latitude", DoubleType(), True),
                StructField("longitude", DoubleType(), True),
                StructField("number", StringType(), True),
                StructField("on_ground", LongType(), True),
                StructField("origin_airport_iata", StringType(), True),
                StructField("registration", StringType(), True),
                StructField("squawk", StringType(), True),
                StructField("time", LongType(), True),
                StructField("vertical_speed", LongType(), True),
            ]
        )
        list_of_flight = spark.createDataFrame([], schema=data_schema)
        flight_list = []
        print(f"Started fetching the flight details via API calls : {datetime.now(paris_tz).strftime(tf)}")
        while boundlist:
            error_list = []
            for bound in boundlist:
                try:
                    flight = fr_api.get_flights(bounds=bound)
                    flight_len= len(flight)
                    if 0 < flight_len < 1500:
                        flight_list.extend(flight)
                    if flight_len == 1500:
                        error_list.append(bound)
                except Exception as error:
                    print(f"Error Occured: {error}")
                    error_list.append(bound)
            print(f"Still Fetching records slowness in API calls, requiore more time : {datetime.now(paris_tz).strftime(tf)}")
            boundlist = []
            for bound in error_list:
                y1, y2, x1, x2 = map(float, bound.split(','))
                smaller_bounds = self.split_coordinates2(y1, y2, x1, x2, 2, 2)
                boundlist.extend(smaller_bounds)
        print(f"Finished fetching flight details : {datetime.now(paris_tz).strftime(tf)}")
        df_flight = spark.createDataFrame(flight_list)         
        list_of_flight_sl = df_flight.select(
            "id",
            "callsign",
            "number",
            "registration",
            "aircraft_code",
            "airline_icao",
            "origin_airport_iata",
            "destination_airport_iata",
            "on_ground",
        )
        return list_of_flight_sl

    def retrieve_airports(self, fr_api):
        airports = fr_api.get_airports()
        airport_schema = StructType([
           StructField("airport_iata", StringType(), True),
           StructField("airport_icao", StringType(), True),
           StructField("airport_name", StringType(), True),
           StructField("airport_country", StringType(), True),
           StructField("airport_latitude", DoubleType(), True),
           StructField("airport_longitude", DoubleType(), True)
        ])

        airport_data = [
            (
                airport.iata,
                airport.icao,
                airport.name,
                airport.country,
                float(airport.latitude),
                float(airport.longitude),
            )
            for airport in airports
        ]
        return spark.createDataFrame(airport_data, airport_schema)

    def combine_airport(self, lf_airline, airport_continent):
        flight_airport = lf_airline.join(
            airport_continent.alias("origin"),
            lf_airline.origin_airport_iata == airport_continent.airport_iata,
            "left",
        ).select(
            lf_airline["*"],
            F.col("origin.airport_name").alias("origin_airport_name"),
            F.col("origin.airport_country").alias("origin_airport_country"),
            F.col("origin.continent").alias("origin_airport_continent"),
            F.col("origin.airport_latitude").alias("origin_airport_latitude"),
            F.col("origin.airport_longitude").alias("origin_airport_longitude"),
        )
        
        flight_airport = flight_airport.join(
            airport_continent.alias("destination"),
            flight_airport.destination_airport_iata == airport_continent.airport_iata,
            "left",
        ).select(
            flight_airport["*"],
            F.col("destination.airport_name").alias("destination_airport_name"),
            F.col("destination.airport_country").alias("destination_airport_country"),
            F.col("destination.continent").alias("destination_airport_continent"),
            F.col("destination.airport_latitude").alias("destination_airport_latitude"),
            F.col("destination.airport_longitude").alias("destination_airport_longitude"),
        )
        return flight_airport
    
    def selected_zones(self, fr_api):
        zones = fr_api.get_zones()
        modified_zones = {}
        for zone_name, zone_data in zones.items():
            modified_zone = {
                "tl_y": zone_data["tl_y"],
                "tl_x": zone_data["tl_x"],
                "br_y": zone_data["br_y"],
                "br_x": zone_data["br_x"],
            }
            modified_zones[zone_name] = modified_zone

        key_mapping = {
            "europe": "Europe",
            "northamerica": "North America",
            "southamerica": "South America",
            "oceania": "Oceania",
            "asia": "Asia",
            "africa": "Africa",
            "atlantic": "Atlantic",
            "maldives": "Maldives",
            "northatlantic": "North Atlantic",
        }
        modified_zones = {
            key_mapping.get(key, key): value for key, value in modified_zones.items()
        }
        return modified_zones

    def remove_duplicates(self, list_of_flight):
        lf_dist = list_of_flight.distinct()
        window_spec = Window().partitionBy("id")
        lf_dist = (
            lf_dist.withColumn("callsign", F.first("callsign", ignorenulls=True).over(window_spec))
            .withColumn("number", F.first("number", ignorenulls=True).over(window_spec))
            .withColumn("registration", F.first("registration", ignorenulls=True).over(window_spec))
            .withColumn("aircraft_code", F.first("aircraft_code", ignorenulls=True).over(window_spec))
            .withColumn("airline_icao", F.first("airline_icao", ignorenulls=True).over(window_spec))
            .withColumn("origin_airport_iata", F.first("origin_airport_iata", ignorenulls=True).over(window_spec))
            .withColumn("destination_airport_iata", F.first("destination_airport_iata", ignorenulls=True).over(window_spec))
            .withColumn("on_ground", F.min("on_ground").over(window_spec))
        )
        lf_dist = lf_dist.distinct()
        return lf_dist
    
    def fetch_airline(self, flight_df):
        airline_details = fr_api.get_airlines()
        airlines_df = spark.createDataFrame(airline_details)
        airlines_df = airlines_df.select("ICAO", F.col("Name").alias("airline_name"))
        lf_airline_joined = lf_dist.join(airlines_df, lf_dist.airline_icao == airlines_df.ICAO, "left").select(lf_dist["*"], "airline_name")
        
        return lf_airline_joined
    
    def retrieve_aircraft_details(self, aircraft_code, airline_icao_in, error_schema):
        try:
            flight_data = fr_api.get_flights(aircraft_type = aircraft_code, airline = airline_icao_in, details=True)
            return flight_data, None

        except CloudflareError as cf_error:
            print(f"CloudflareError: {cf_error}")
            print(f"aircraft_code : {aircraft_code} and airline_icao_in : {airline_icao_in}")
            new_row = Row(aircraft_code=aircraft_code, airline_icao=airline_icao_in)
            error_df = spark.createDataFrame([new_row], schema=error_schema)
            return None, error_df

        except Exception as error:
            print(f"Error Occured: {error}")
            print(f"aircraft_code : {aircraft_code} and airline_icao_in : {airline_icao_in}")
            new_row = Row(aircraft_code=aircraft_code, airline_icao=airline_icao_in)
            error_df = spark.createDataFrame([new_row], schema=error_schema)
            return None, error_df

    def get_aircraft_model(self, flight_airport):
        aircraft_airline = (
            flight_airport.groupBy("aircraft_code")
            .agg(F.first("airline_icao", ignorenulls=True).alias("airline_icao"))
            .filter(~F.col("aircraft_code").isin("N/A") & ~F.col("aircraft_code").isin("") & ~F.col("airline_icao").isin(""))
        )
        required_columns = ["aircraft_code", "aircraft_model"]
        data_schema = StructType(
            [
                StructField("aircraft_code", StringType(), True),
                StructField("aircraft_model", StringType(), True),
            ]
        )
        error_schema = StructType(
            [
                StructField("aircraft_code", StringType(), True),
                StructField("airline_icao", StringType(), True),
            ]
        )
        flight_data_df = spark.createDataFrame([], schema=data_schema)
        print(f"Started collecting aircraft details - {datetime.now(paris_tz).strftime(tf)}")
        
        while not aircraft_airline.isEmpty():
            error_df = spark.createDataFrame([], schema=error_schema)
            for aircraft_code, airline_icao in aircraft_airline.select("aircraft_code", "airline_icao").collect():
                flight_data, temp_error_df = self.retrieve_aircraft_details(aircraft_code, airline_icao, error_schema)
                if flight_data:
                    temp_df = spark.createDataFrame(
                        [
                            Row(
                                **{
                                    col: None if getattr(flight, col) == "N/A" else getattr(flight, col) 
                                    for col in required_columns
                                }
                            )
                            for flight in flight_data
                        ],
                        schema = data_schema,
                    )
                    flight_data_df = flight_data_df.union(temp_df)
                
                if temp_error_df is not None:
                    error_df = error_df.union(temp_error_df)
                    
            print(f"API call issues, retying again - {datetime.now(paris_tz).strftime(tf)}")
            aircraft_airline = error_df
            print(f"aircraft_airline count : {aircraft_airline.count()}, error_df count : {error_df.count()}")
        print(f"Finished fetching the data - {datetime.now(paris_tz).strftime(tf)}")
        print(f"Counting the number of records in the flight_data_df : {flight_data_df.count()} - {datetime.now(paris_tz).strftime(tf)}")
        flight_data_dist = flight_data_df.dropDuplicates(["aircraft_code"])
        print(f"Counting the number of records in the flight_data_dist : {flight_data_dist.count()} - {datetime.now(paris_tz).strftime(tf)}")

        flight_data_dist = flight_data_dist.withColumn("aircraft_model_short", F.split(F.col("aircraft_model"), " ")[0])
        aircraft_airline_comb = flight_airport.join(
            flight_data_dist, 
            flight_airport.aircraft_code == flight_data_dist.aircraft_code, 
            "left",
            ).select(flight_airport["*"], "aircraft_model", "aircraft_model_short")
        
        print(f"Counting the number of records after join- aircraft_airline_comb: {flight_data_dist.count()} - {datetime.now(paris_tz).strftime(tf)}")
        return aircraft_airline_comb

# COMMAND ----------


