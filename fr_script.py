# Databricks notebook source
from FlightRadar24.errors import CloudflareError
from FlightRadar24 import FlightRadar24API
import pyspark.sql.functions as F
from pyspark.sql import Window
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DoubleType, FloatType, ShortType
from pyspark.sql import Row
from datetime import datetime
from flight_radar_utils import FlightRadarFunction
import pycountry_convert as pc
import pytz
import time
from math import cos, sin, sqrt, atan2, pi, asin

# COMMAND ----------

# MAGIC %run "./flight_radar_utils"

# COMMAND ----------

# Initializing Object
fr_api = FlightRadar24API()
fr = FlightRadarFunction()

# COMMAND ----------

# Variable declaration
paris_tz = pytz.timezone("Europe/Paris")
tf = "%Y-%m-%d %H:%M:%S"
y1, y2, x1, x2 = 90, -90, 180, -180
rows = 18
cols = 18

# COMMAND ----------

# Fetch and Preparing the Flight data
coordinates = fr.split_coordinates2(y1, y2, x1, x2, rows, cols)
list_of_flight = fr.get_flights_list(coordinates)

lf_dist = fr.remove_duplicates(list_of_flight)
#lf_dist.show()

lf_airline = fr.fetch_airline(lf_dist)
# lf_airline.show()

# COMMAND ----------

airport_df = fr.retrieve_airports(fr_api)

def country_to_continent(country_name):
    try:
        country_alpha2 = pc.country_name_to_country_alpha2(country_name)
        country_continent_code = pc.country_alpha2_to_continent_code(country_alpha2)
        country_continent_name = pc.convert_continent_code_to_continent_name(country_continent_code)
        return country_continent_name
    
    except:
        return None
    # ?? pc.country_name_to_country_alpha2
country_to_continent_udf = udf(country_to_continent, StringType())
airport_continent = airport_df.withColumn("continent", country_to_continent_udf("airport_country"))
# airport_continent.show()

flight_airport = fr.combine_airport(lf_airline, airport_continent)
print(flight_airport.count())
# flight_airport.show()


# COMMAND ----------

flight_data_df = fr.get_aircraft_model(flight_airport)
# flight_data_df.show()

# COMMAND ----------

def get_distance(lat1, lon1, lat2, lon2):
    if lat1 is None or lon1 is None or lat2 is None or lon2 is None:
        return 0.0
    r = 6371 # km Earth radius - r = 3959  # miles Earth radius
    p = pi / 180

    a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p) * cos(lat2*p) * (1-cos((lon2-lon1)*p))/2
    distance = 2 * r * asin(sqrt(a))
    return distance

# COMMAND ----------

dist_udf = udf(get_distance, FloatType())
flight_details_df = flight_data_df.withColumn(
    "flight_route_distance",
    dist_udf(
        F.col("origin_airport_latitude"),
        F.col("origin_airport_longitude"),
        F.col("destination_airport_latitude"),
        F.col("destination_airport_longitude")
    ),
)
#print(flight_details_df.count())
# flight_details_df.show()

# COMMAND ----------

max_airline = (
    flight_details_df.groupBy("airline_name")
    .agg(F.count("*").alias("number_of_flights"))
    .filter(~F.col("airline_name").isin(""))
    .orderBy(F.desc("number_of_flights"))
)
top_airline = max_airline.select("*").first()
# print(type(top_airline))   #<class 'pyspark.sql.types.Row'>
# top_airline.show()

airline_name = top_airline.airline_name 
number_of_flights = top_airline.number_of_flights 
print(f"{airline_name} is most active airline with {number_of_flights} active flights")
#American Airlines is most active airline with 932 active flights

# COMMAND ----------

window_func = Window.orderBy(F.desc("flight_route_distance"))
max_distance_flight = flight_details_df.withColumn("rank", F.rank().over(window_func)).filter("rank == 1")

# Collect the rows as a list
max_distance_rows = max_distance_flight.collect()

print("Below are the flights with the longest route:")
for row in max_distance_rows:
    flight_id = row["id"]
    airline_name = row["airline_name"]
    origin_airport_name = row["origin_airport_name"]
    destination_airport_name = row["destination_airport_name"]
    flight_route_distance = round(row["flight_route_distance"], 2)

    print(f"flight number - {flight_id} of {airline_name} which has originated from {origin_airport_name} and destination is {destination_airport_name} has flight length of {flight_route_distance} km")

# COMMAND ----------

same_continent_df = flight_details_df.select(F.col("origin_airport_continent").alias("continent"), "airline_name", "flight_route_distance").filter(
    (F.col("origin_airport_continent") == F.col("destination_airport_continent")) &
    ~F.col("origin_airport_continent").isin("") #& ~F.col("airline_name").isin("") 
)
# same_continent_df.show()

# COMMAND ----------

average_flight_length = same_continent_df.filter(F.col("flight_route_distance") != 0).groupBy("continent").agg(
    F.round(F.avg("flight_route_distance"), 2).alias("average_flight_length")
)
print("Average Flight length for each continent in km : ")
# average_flight_length.show()

average_flight_length_rows = average_flight_length.collect()

for row in average_flight_length_rows:
    continent = row["continent"]
    average_flight_length = row["average_flight_length"]

    print(f"In {continent} average flight length is {average_flight_length} km")


# COMMAND ----------

# most active airline in each continent
most_active_airline = (
    same_continent_df.groupBy("continent", "airline_name")
    .agg(F.count("*").alias("number_of_flights"))
    .filter(~F.col("airline_name").isin(""))
)
window_func = Window().partitionBy("continent").orderBy(F.desc("number_of_flights"))
most_active_airline = most_active_airline.withColumn("rank", F.rank().over(window_func)).filter(F.col("rank") == 1)     #.drop("rank")

print("For each continent, the airline with the most active regional flights : ")
# most_active_airline.show()
most_active_airline_rows = most_active_airline.collect()

for row in most_active_airline_rows:
    continent = row["continent"]
    airline_name = row["airline_name"]
    number_of_flights = row["number_of_flights"]

    print(f"In {continent} most active airline is {airline_name} with {number_of_flights} active flights")

# COMMAND ----------

most_active_aircraft = flight_details_df.groupBy("aircraft_model_short").agg(F.count("*").alias("number_of_flight")).filter(~F.col("aircraft_model_short").isin(""))
window_func = Window.orderBy(F.desc("number_of_flight"))

most_active_aircraft = most_active_aircraft.withColumn("rank", F.rank().over(window_func)).filter("rank == 1")
most_active_aircraft_rows = most_active_aircraft.collect()
print("The aircraft manufacturing company with the most active flights :")
# most_active_aircraft.show()
for row in most_active_aircraft_rows:
    aircraft_model_short = row["aircraft_model_short"]
    number_of_flight = row["number_of_flight"]

    print(f"{aircraft_model_short} manufacturing company has most flight with {number_of_flight}")


# COMMAND ----------

aircraft_model  = flight_details_df.groupBy("origin_airport_country", "aircraft_model").agg(F.count("*").alias("number_of_aircraft")).filter(~F.col("origin_airport_country").isin("") & ~F.col("aircraft_model").isin("")) #.orderBy("origin_airport_country", F.desc( "number_of_aircraft"))
window_func = Window().partitionBy("origin_airport_country").orderBy(F.desc("number_of_aircraft"))
aircraft_model = aircraft_model.withColumn("rank", F.dense_rank().over(window_func)).filter(F.col("rank").cast("int") <= 3).drop("rank")
print("For each country of the airline, the top 3 aircraft models in use:")
display(aircraft_model)



'''
Note
# Two airlines can have same airline iata, and its also duplicates in the fr_api.get_airlines(), so avoid using iata, use only icao - 2I, 5B, 4K
# https://www.iata.org/en/publications/directories/code-search
list_of_flight = list_of_flight.withColumn(
    "airline_code",
    F.when(col("airline_icao").isNotNull(), col("airline_icao")).otherwise(
        col("airline_iata")
    ),
)
'''
